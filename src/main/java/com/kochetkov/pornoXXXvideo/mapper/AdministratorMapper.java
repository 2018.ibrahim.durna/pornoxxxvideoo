package com.kochetkov.pornoXXXvideo.mapper;

import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorCreateDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorFullDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorUpdateDto;
import com.kochetkov.pornoXXXvideo.entity.Administrator;

import java.util.ArrayList;
import java.util.List;

public class AdministratorMapper {

    public List<AdministratorPreviewDto> mapToDtoList(List<Administrator> entities) {
        List<AdministratorPreviewDto> dtos = new ArrayList<>();

        for (Administrator entity : entities) {
            AdministratorPreviewDto dto = new AdministratorPreviewDto();

            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setRole(entity.getRole());
            dtos.add(dto);
        }
        return dtos;
    }

    public Administrator mapToEntity(AdministratorCreateDto createDto) {
        Administrator administrator = new Administrator();
        administrator.setName(createDto.getName());
        administrator.setSurname(createDto.getSurname());
        administrator.setEmail(createDto.getEmail());
        administrator.setPassword(createDto.getPassword());

        return administrator;
    }

    public Administrator mapToEntity(AdministratorUpdateDto updateDto) {
        Administrator administrator = new Administrator();
        administrator.setName(updateDto.getName());
        administrator.setSurname(updateDto.getSurname());
        administrator.setEmail(updateDto.getEmail());
        administrator.setPassword(updateDto.getPassword());

        return administrator;
    }

    public AdministratorFullDto mapToDto(Administrator entity) {
        AdministratorFullDto fullDto = new AdministratorFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setSurname(entity.getSurname());
        fullDto.setEmail(entity.getEmail());
        fullDto.setRole(entity.getRole());

        return fullDto;
    }
}
