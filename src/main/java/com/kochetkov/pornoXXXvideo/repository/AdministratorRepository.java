package com.kochetkov.pornoXXXvideo.repository;

import com.kochetkov.pornoXXXvideo.entity.Administrator;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdministratorRepository {

    Administrator findByEmail(String email);

    List<Administrator> findAll();

    Administrator findById(Long id);

    Administrator create(Administrator administrator);

    Administrator update(Administrator administrator);

    void deleteById(Long id);

    void deleteAll();
}
