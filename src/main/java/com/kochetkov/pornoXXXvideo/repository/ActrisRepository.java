package com.kochetkov.pornoXXXvideo.repository;

import com.kochetkov.pornoXXXvideo.entity.Actris;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActrisRepository {

    Actris findByEmail(String string);

    List<Actris> findAll();

    Actris findById(Long id);

    Actris create(Actris admin);

    Actris update(Actris admin);

    void deleteById(Long id);

    void deleteAll();

}
