package com.kochetkov.pornoXXXvideo.dto.actrisDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActrisCreateDto {

    private String name;

    private String surname;

    private int experience;

    private String category;

    private String achievements;

    private String email;

    private String password;

}
