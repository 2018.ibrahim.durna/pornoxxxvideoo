package com.kochetkov.pornoXXXvideo.dto.commentDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentCreateDto {

    private String message;

    private String name;

    private String surname;

    private String email;

    private int mark;

    private Long actrisId;
}
