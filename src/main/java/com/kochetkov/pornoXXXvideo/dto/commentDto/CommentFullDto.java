package com.kochetkov.pornoXXXvideo.dto.commentDto;

import com.kochetkov.pornoXXXvideo.entity.Actris;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentFullDto {
    private Long id;

    private Actris actris;

    private String message;

    private String name;

    private String surname;

    private String email;

    private int mark;

    private boolean published;

}
