package com.kochetkov.pornoXXXvideo.service;

import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorLoginDto;
import com.kochetkov.pornoXXXvideo.entity.Administrator;
import com.kochetkov.pornoXXXvideo.entity.enums.Role;
import org.springframework.stereotype.Service;

@Service
public interface AutAdminsrtatorService {

    void login(AdministratorLoginDto loginDto);

    void logOut();

    boolean isAuthenticated();

    Role getRole();

    Administrator getLoginedAdmin();
}
