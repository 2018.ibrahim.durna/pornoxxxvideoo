package com.kochetkov.pornoXXXvideo.service;

import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorCreateDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorFullDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorUpdateDto;
import com.kochetkov.pornoXXXvideo.entity.Administrator;
import com.kochetkov.pornoXXXvideo.exception.InvalidDtoException;
import com.kochetkov.pornoXXXvideo.exception.MissedUpdateldException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdministratorService {

    AdministratorFullDto findByEmail(String email);

    List<AdministratorPreviewDto> findAll();

    AdministratorFullDto findById(Long id);

    AdministratorFullDto create(AdministratorCreateDto createDto);

    AdministratorFullDto update(AdministratorUpdateDto updateDto);

    void deleteById(Long id);
}
