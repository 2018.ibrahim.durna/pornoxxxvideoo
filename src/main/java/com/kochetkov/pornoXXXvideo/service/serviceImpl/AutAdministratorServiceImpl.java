package com.kochetkov.pornoXXXvideo.service.serviceImpl;

import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorLoginDto;
import com.kochetkov.pornoXXXvideo.entity.Administrator;
import com.kochetkov.pornoXXXvideo.entity.enums.Role;
import com.kochetkov.pornoXXXvideo.repository.AdministratorRepository;
import com.kochetkov.pornoXXXvideo.repository.repositoryImpl.AdministaratorRepositoryImpl;
import com.kochetkov.pornoXXXvideo.service.AutAdminsrtatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AutAdministratorServiceImpl implements AutAdminsrtatorService {

    private Administrator loginAdministrator;
    @Autowired
    private AdministratorRepository administratorRepository = new AdministaratorRepositoryImpl();

    @Override
    public void login(AdministratorLoginDto loginDto) {
        Administrator foundAdministrator = administratorRepository.findByEmail(loginDto.getEmail());
        if (foundAdministrator == null) {
            throw new RuntimeException("Admin not found by email: " + loginDto.getEmail());
        }

        if (!foundAdministrator.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("User password is incorrect");
        }

        loginAdministrator = foundAdministrator;
    }

    @Override
    public void logOut() {
        loginAdministrator = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginAdministrator != null;
    }

    @Override
    public Role getRole() {
        return loginAdministrator.getRole();
    }

    @Override
    public Administrator getLoginedAdmin() {
        return loginAdministrator;
    }
}
