package com.kochetkov.pornoXXXvideo.service;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisCreateDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisFullDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisUpdateDto;
import com.kochetkov.pornoXXXvideo.exception.InvalidDtoException;
import com.kochetkov.pornoXXXvideo.exception.MissedUpdateldException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ActrisService {

    List<ActrisPreviewDto> findAll();

    ActrisFullDto findById(Long id);

    ActrisFullDto create(ActrisCreateDto createDto);

    ActrisFullDto update(ActrisUpdateDto updateDto);

    void deleteById(Long id);
}
