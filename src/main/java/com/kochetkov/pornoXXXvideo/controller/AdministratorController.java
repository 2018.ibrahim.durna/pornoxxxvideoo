package com.kochetkov.pornoXXXvideo.controller;

import com.kochetkov.pornoXXXvideo.dto.actrisDto.ActrisPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorCreateDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorFullDto;
import com.kochetkov.pornoXXXvideo.dto.administratorDto.AdministratorLoginDto;
import com.kochetkov.pornoXXXvideo.service.ActrisService;
import com.kochetkov.pornoXXXvideo.service.AdministratorService;
import com.kochetkov.pornoXXXvideo.service.AutAdminsrtatorService;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.ActrisServiceImpl;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.AdministratorServiceImpl;
import com.kochetkov.pornoXXXvideo.service.serviceImpl.AutAdministratorServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class AdministratorController {

    private AdministratorService administratorService = new AdministratorServiceImpl();

    private AutAdminsrtatorService autAdminsrtatorService = new AutAdministratorServiceImpl();

    private ActrisService actrisService = new ActrisServiceImpl();

    @RequestMapping(method = RequestMethod.GET, value = "/admin/registration")
    public String openRegistrationPage(Model model) {
        model.addAttribute("createDto", new AdministratorCreateDto());
        return "admin-registration";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/create")
    public String saveAdmin(AdministratorCreateDto createDto) {
        AdministratorFullDto created = administratorService.create(createDto);
        return "redirect:/index-for-admin";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/sign-in")
    public String loginAdmin(Model model) {
        model.addAttribute("loginDto", new AdministratorLoginDto());
        return "admin-sign-in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/login")
    public String login(AdministratorLoginDto loginDto) {
        autAdminsrtatorService.login(loginDto);

        return "redirect:/index-for-admin";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index-for-admin")
    public String openMainPage(Model model) {
        if (!autAdminsrtatorService.isAuthenticated()) {
            return "redirect:/oops"; //TODO не обрабатывает ошибку
        }

        List<ActrisPreviewDto> found = actrisService.findAll();

        model.addAttribute("all_actrises", found);
        model.addAttribute("logined_administrator", autAdminsrtatorService.getLoginedAdmin());
        return "index-for-admin";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/oops")
    public String showErrorPage() {
        return "exception";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/logout")
    public String logOut() {
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/logout")
    public String logout() {
        autAdminsrtatorService.logOut();
        return "redirect:/index";
    }

}
